package com.example.medigraphyecgintegration;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    AlertDialog.Builder builder;
    public static String device_address, deviceName;
    private static final UUID MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    ListView list_view;
    TextView txt_start_test;
    ProgressDialog ScanDialog;
    ProgressDialog DataDialog;
    BluetoothAdapter bAdapter;
    IntentFilter filter;
    BroadcastReceiver receiver;
    BluetoothDevice device1;
    private ArrayAdapter<String> listAdapter;
    ArrayList<BluetoothDevice> devices;

    private static final int REQUEST_ENABLE_BT = 0;
    protected static final int SUCCESS_CONNECT = 0;
    protected static final int SEND_INSTREAM = 1;
    protected static final int SEND_OUTSTREAM = 2;
    protected static final int CONNECTION_ERROR_GENERAL = 3;
    //protected static final int MESSAGE_COMMUNICATION = 4;
    //protected static final int RECONNECT = 5;
    protected static final int CONNECTION_ERROR_DEVICE = 10;
    private static int DEVICE_FOUND = 0;
    int BLUETOOTH_ADD_LENGTH = 17;
    public boolean started = false;
    public static boolean stop = false, demo_start = false, refresh_data = false, menu_press = false, create_image = false,
            create_pdf = false, create_data = false, create_text = false, lead_pressed = false, loggerCreated = false;
    InputStream instrm;
    OutputStream outstrm;
    public static String mState = "HIDE_MENU";
    public boolean load;
    final AnimatorSet mAnimationSet = new AnimatorSet();
    paintView pv;
    public static int iScreenWidth, iScreenHeight;
    Display d;
    protected static final int START_DATA_ACQ = 1;
    protected static final int STOP_DATA = 2;
    protected static final int GENERATE_REPORT = 3;
    protected static final int LOAD_DATA = 5;
    protected static final int MESSAGE_COMMUNICATION = 6;
    protected static final int RECONNECT = 7;
    protected static final int NEW_PATIENT = 8;
    public static int f_size, no_of_lead_to_display, total_pages,
            lead_page = 0, step, selectedPosition, item_id_acq_mode,
            item_id_gain, item_id_filter, mode;
    public static String selectedGain = "10mm/mv";
    public static int report_sequential, iGain;
    private boolean REGISTER = false;
    boolean deviceselected = false;
    int strcounter = 0;
    public static BluetoothDevice selectedDevice;

    public boolean next_and_prev_clicked = false,
            menu_clicked = false;
    public static boolean report_gen = false,
            filter_change = false,
            gain_change = false,
            load_existing_file = false,
            send_report = false,
            acq_repeat = false,
            acq_mode_change = false;

    @SuppressWarnings("deprecation")
    private void initialize_GUI() {
        try {
            //to run application full screen
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            //to keep the screen on while application is running
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            //to change the text color of title
            int actionBarTitleId = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
//            if (actionBarTitleId > 0) {
//                title = (TextView) findViewById(actionBarTitleId);
//                if (title != null)
//                    title.setTextColor(Color.WHITE);
//            }
            //set the color of titlebar
            ActionBar ab = getActionBar();
            ab.setDisplayUseLogoEnabled(false);

            //ab.setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bar_background_color));
            ab.hide();
            //myVib = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);

            //initialize GUI components
//            bt_new = (Button) findViewById(R.id.new_main_activity_record_test_btn);
//            bt_new.setOnClickListener(this);
//            bt_load = (Button) findViewById(R.id.new_main_activity_view_report_btn);
//            bt_load.setOnClickListener(this);
//            bt_lead = (Button) findViewById(R.id.new_main_activity_select_lead_btn);
//            bt_lead.setOnClickListener(this);
//            bt_view = (Button) findViewById(R.id.view_button);
//            bt_view.setOnClickListener(this);

            //getting the height and width of tablet/mobile
            d = getWindowManager().getDefaultDisplay();
            iScreenWidth = d.getWidth();
            iScreenHeight = d.getHeight();
            Log.i("medtel-debug", "initialize_GUI() completed");
        } catch (Exception e) {
            Log.e("medtel-debug", "Error in initialize_GUI()", e.getCause());
            e.printStackTrace();
        }
    }

    public static MenuItem device_detail_menu, gain_menu, filter_menu, add_patient_info, load_menu, next_menu, demo_menu, prev_menu, view_report,
            mode_menu, battery, manual_menu, new_patient, export_to_ascii, acq_mode, pg_one, pg_two, pg_three, pg_four, next_lead, prev_lead, repeat_acq;
    public static String battery_status = "N/A", v1_status = "Not Connected", v6_status = "Not Connected",
            v2_status = "Not Connected", v3_status = "Not Connected", v4_status = "Not Connected",
            v5_status = "Not Connected", ll_status = "Not Connected", la_status = "Not Connected",
            ra_status = "Not Connected";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initLayout();
        initListener();
        initialize_GUI();
    }

    private void initLayout() {

        list_view = findViewById(R.id.list_view);
        txt_start_test = findViewById(R.id.txt_start_test);
        // builder = new AlertDialog.Builder(this);


        listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, 0);
        devices = new ArrayList<BluetoothDevice>();
        list_view.setAdapter(listAdapter);
        list_view.setOnItemClickListener(this);
        bAdapter = BluetoothAdapter.getDefaultAdapter();

        ScanDialog = new ProgressDialog(this); // this = YourActivity
        ScanDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        ScanDialog.setTitle("Searching");
        ScanDialog.setMessage("Searching for nearby ECG devices...");
        ScanDialog.setIndeterminate(true);
        ScanDialog.setCanceledOnTouchOutside(false);

        DataDialog = new ProgressDialog(this); // this = YourActivity
        DataDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        DataDialog.setTitle("Connecting");
        DataDialog.setMessage("Please wait for device to connect...");
        DataDialog.setIndeterminate(true);
        DataDialog.setCanceledOnTouchOutside(false);


    }

    private void initListener() {

        txt_start_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (list_view.getVisibility() == View.GONE) {
                    list_view.setVisibility(View.VISIBLE);
                }

                checkBluetoothStatus();

            }
        });

    }

    //function to check initial Bluetooth status
    public void checkBluetoothStatus() {
        try {
            if (bAdapter == null)
                Toast.makeText(getApplicationContext(), "NO BLUETOOTH DEVICE DETECTED!", Toast.LENGTH_SHORT).show();

            if (bAdapter.isEnabled()) {
                System.out.println("checkBluetoothStatus");
                Log.d("medtel-debug", " check bluetooth status");
                doDiscovery();
                init();
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        } catch (Exception e) {
            System.out.println("bt status>" + e.getMessage());
        }
    }

    private void doDiscovery() {
        ScanDialog.show();

        // If we're already discovering, stop it
        if (bAdapter.isDiscovering()) {
            bAdapter.cancelDiscovery();
        }
        System.out.println("doDiscovery");
        // Request discover from BluetoothAdapter
        bAdapter.startDiscovery();
    }

    private void init() {
        try {
            filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            receiver = new BroadcastReceiver() {
                @SuppressWarnings("static-access")
                @Override
                public void onReceive(Context arg0, Intent intent) {
                    String action = intent.getAction();
                    Log.d("medtel-debug", "broadcast receiver on receive action = " + action);
                    String deviceNameRecieved;


                    //	read_bluetooth_address();//Moved to onCreate
                    if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                        System.out.println("actio>>>n" + action);
                        Log.d("medtel-debug", "action found = " + action);
                        setProgressBarVisibility(false);

                        device1 = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        System.out.println(device1);
                        Log.d("medtel-debug", "action found = " + action);
                        Log.d("medtel-debug", "device1 = "  +device1);
                        Log.d("medtel-debug", "device name = " + device1.getName());
                        deviceNameRecieved = device1.getName();

                        //ScanDialog.hide();

                        if (listAdapter.isEmpty()) {

                            System.out.println("empty");
                            Log.d("medtel-debug", "empty");
                            //Populate only those devices which are ECG Recorders
                            try {
                                if (deviceNameRecieved != null && deviceNameRecieved.startsWith("ECGREC-")) {
                                    //if device is found then flag DEVICE_FOUND is set 1
                                    //System.out.println("deviceNameRecieved" + deviceNameRecieved);
                                    DEVICE_FOUND = 1;
                                    deviceName = deviceNameRecieved;
                                    //Log.i("device_address", device_address);

                                    Log.d("medtel-debug", "device name received = " + deviceNameRecieved);
                                    Log.d("medtel-debug", "device address = " + device_address);
//									if(device_address.equals("") || device_address.equals(null))// there is no entry in the bluetooth file, so add the device to list.
//									{
//										System.out.println("deviceName"+deviceName);
//										listAdapter.add(deviceName);
//									}
                                    if (device1.getAddress().toString().equals(device_address)) {
                                        bAdapter.cancelDiscovery();
                                        ScanDialog.hide();

                                        String demo = "ECGREC-DEMO201020";

                                        if (deviceNameRecieved.startsWith("ECGREC-DEMO")) {
                                            Log.d("medtel-debug", "ECG Demo");

                                            String inputDate = deviceNameRecieved.replaceAll("[^0-9]", "");

                                            SimpleDateFormat inputdate = new SimpleDateFormat("yyMMdd");
                                            SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                                            Date outDate = inputdate.parse(inputDate);
                                            String outputText = outputFormat.format(outDate);
                                            String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());

                                            Date current = outputFormat.parse(currentDate);
                                            Date outPut = outputFormat.parse(outputText);

                                            if (outPut.equals(current) || outPut.compareTo(current) < 0) {

                                                builder.setMessage("Please return the Demo piece to the company").setCancelable(false);
                                                builder.setTitle("Alert!");
                                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                    }
                                                });

                                                AlertDialog alert = builder.create();
                                                alert.show();

                                            } else if (current.compareTo(outDate) < 0) {

                                                long remainingTime = outDate.getTime() - current.getTime();
                                                long days = remainingTime / (24 * 60 * 60 * 1000);

                                                builder.setMessage("You have " + days + " days left for demo ").setCancelable(false);
                                                builder.setTitle("Alert!");

                                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        ConnectThread connect = new ConnectThread(device1);
                                                        connect.start();
                                                    }
                                                });
                                                AlertDialog alert = builder.create();

                                                alert.show();
                                            }
                                        } else {
                                            Log.d("medtel-debug", "connect thread");
                                            ConnectThread connect = new ConnectThread(device1);
                                            connect.start();
                                        }
                                    }
                                    devices.add(device1);
                                    listAdapter.add(deviceName);
                                    ScanDialog.hide();

                                } else {
                                    //do nothing.. continue with search
                                    Log.d("medtel-debug", " device is null");
                                }

                            } catch (Exception e) {
                                System.out.println("error in bluetooth init" + e.getMessage());
                                Log.d("medtel-debug", "error in bluetooth init" +e.getMessage() );
                            }

                        } else {
                            Log.e("list_count", String.valueOf(list_view.getCount()));
                            Log.d("medtel-debug", " list count = " + String.valueOf(list_view.getCount()));
                            for (int a = 0; a < list_view.getCount(); a++) {
                                //if not exist in list then add in list
                                if (deviceNameRecieved != null) {


                                    if (deviceNameRecieved.startsWith("ECGREC-") && !devices.contains(deviceNameRecieved))
                                        //Make sure only one ECG machine is in at a time. If two devices are present their name has to be different.
                                    {
                                        deviceName = deviceNameRecieved;


                                        if (device1.getAddress().toString().equals(device_address) && a == 0)    //a=0 in list is the last element added... List is Last IN First Out

                                        {
                                            bAdapter.cancelDiscovery();
                                            ScanDialog.hide();

                                            String demo = "ECGREC-DEMO201020";

                                            if (deviceNameRecieved.startsWith("ECGREC-DEMO")) {

                                                String inputDate = deviceNameRecieved.replaceAll("[^0-9]", "");

                                                SimpleDateFormat inputdate = new SimpleDateFormat("yyMMdd");
                                                SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                                                Date outDate = null;
                                                try {
                                                    outDate = inputdate.parse(inputDate);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                                String outputText = outputFormat.format(outDate);
                                                String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());

                                                Date current = null;
                                                Date outPut = null;
                                                try {
                                                    current = outputFormat.parse(currentDate);
                                                    outPut = outputFormat.parse(outputText);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();

                                                }

                                                if (outPut.equals(current) || outPut.compareTo(current) < 0) {

                                                    builder.setMessage("Please return the Demo piece to the company").setCancelable(false);
                                                    builder.setTitle("Alert!");
                                                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            dialogInterface.dismiss();
                                                        }
                                                    });

                                                    AlertDialog alert = builder.create();
                                                    alert.show();

                                                } else if (current.compareTo(outDate) < 0) {

                                                    long remainingTime = outDate.getTime() - current.getTime();
                                                    long days = remainingTime / (24 * 60 * 60 * 1000);

                                                    builder.setMessage("You have " + days + " days left for demo ").setCancelable(false);
                                                    builder.setTitle("Alert!");

                                                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            ConnectThread connect = new ConnectThread(device1);
                                                            connect.start();
                                                        }
                                                    });
                                                    AlertDialog alert = builder.create();

                                                    alert.show();
                                                }
                                            } else {

                                                ConnectThread connect = new ConnectThread(device1);
                                                connect.start();

                                            }

                                        } else {
                                            Log.d("medtel-debug", "adding device name to adapter");
                                            listAdapter.add(deviceName);
                                            ScanDialog.hide();
                                        }
                                        devices.add(device1);
                                        System.out.println("add device");
                                        Log.d("medtel-debug", "add device");
                                        break;
                                    }
                                }

                            }
                        }
                        // devices.add(device1);
                        //setTitleColor(Color.BLACK);
                        //setTitle("Select ECGRec Device...");

                    }
                    else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                        Log.d("medtel-debug", "action started = " + action);
                        ScanDialog.show();
                    } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                        Log.d("medtel-debug", "action discovery finished = " + action);
                        ScanDialog.hide();

                        if (DEVICE_FOUND == 0) {

                            Toast.makeText(getApplicationContext(), "No ECG Recorders Found. Check If the Device is Off.", Toast.LENGTH_LONG).show();
                        }
                    } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                        ScanDialog.hide();
                        if (bAdapter.getState() == bAdapter.STATE_OFF) {
                            Intent enableBtIntent = new Intent(
                                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(enableBtIntent,
                                    REQUEST_ENABLE_BT);
                            doDiscovery();
                            init();
                        }
                    }
                }
            };
            registerReceiver(receiver, filter);

            filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            registerReceiver(receiver, filter);

            filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            registerReceiver(receiver, filter);

            filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(receiver, filter);

            REGISTER = true;

        } catch (Exception e) {
            System.out.println("error in bluetooth init" + e.getMessage());
            Toast.makeText(getApplicationContext(), "Bluetooth init error!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
        System.out.println("on item click");
        connect_device(arg2);

    }

    public void connect_device(int arg2) {
        deviceselected = true;
        if (bAdapter.isDiscovering())
            bAdapter.cancelDiscovery();

        try {
            selectedDevice = devices.get(arg2);
            if (!BluetoothAdapter.checkBluetoothAddress(devices.get(arg2).getAddress())) {
                System.out.println("BT not valid");
                Toast.makeText(getApplicationContext(), "Not a valid Bluetooth Address..Select another bluetooth device OR Configure again!!", Toast.LENGTH_SHORT).show();
            } else {
                System.out.println("selectedDevice.getName()" + selectedDevice.getName());
                System.out.println("bAdapter.getBondedDevices()" + bAdapter.getBondedDevices());
                if (bAdapter.getBondedDevices().contains(selectedDevice.getName()))//device_address))//device is already paired, directly start the thread.
                {
                    System.out.println("paired device available, directly connect to device");


                    String demo = selectedDevice.getName();

                    if (demo.startsWith("ECGREC-DEMO")) {

                        String inputDate = demo.replaceAll("[^0-9]", "");

                        SimpleDateFormat inputdate = new SimpleDateFormat("yyMMdd");
                        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                        Date outDate = null;
                        try {
                            outDate = inputdate.parse(inputDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String outputText = outputFormat.format(outDate);
                        String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());

                        Date current = null;
                        Date outPut = null;
                        try {
                            current = outputFormat.parse(currentDate);
                            outPut = outputFormat.parse(outputText);
                        } catch (ParseException e) {
                            e.printStackTrace();

                        }

                        if (outPut.equals(current) || outPut.compareTo(current) < 0) {

                            builder.setMessage("Please return the Demo piece to the company").setCancelable(false);
                            builder.setTitle("Alert!");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();

                        } else if (current.compareTo(outDate) < 0) {

                            long remainingTime = outDate.getTime() - current.getTime();
                            long days = remainingTime / (24 * 60 * 60 * 1000);

                            builder.setMessage("You have " + days + " days left for demo ").setCancelable(false);
                            builder.setTitle("Alert!");

                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    ConnectThread connect = new ConnectThread(selectedDevice);
                                    connect.start();
                                }
                            });
                            AlertDialog alert = builder.create();

                            alert.show();
                        }
                    } else {
                        ConnectThread connect = new ConnectThread(selectedDevice);
                        connect.start();
                    }

                } else {
                    System.out.println("call pair device");
                    pairDevice(selectedDevice);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public final Handler Bt_mHandler = new Handler() {
        public void handleMessage(Message msg) {
            //battery.setTitle("Battery: " + MainActivity.battery_status);

            switch (msg.what) {

                case SUCCESS_CONNECT:
                    try {
                        System.out.println("Device Address to Store: " + device_address);
                        store_bluetooth_address(device_address);
                        DataDialog.hide();
                        ScanDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Successfully Connected to " + deviceName, Toast.LENGTH_LONG).show();
                        System.out.println("SUCCESS_CONNECT");
                        ConnectedThread connectedThread = new ConnectedThread((BluetoothSocket) msg.obj);
                        connectedThread.start();
                    } catch (Exception e1) {
                        Toast.makeText(getApplicationContext(), "Error after connecting to Device through bluetooth", Toast.LENGTH_SHORT).show();
                        System.out.println("Error161=" + e1.getMessage());
                    }
                    //battery.setTitle("Battery: " + MainActivity.battery_status);

                    break;
                case SEND_INSTREAM:
                    instrm = (InputStream) msg.obj;
                    System.out.println("SEND_INSTREAM=" + instrm.toString());
                    // battery.setTitle("Battery: " + MainActivity.battery_status);

                    break;
                case SEND_OUTSTREAM:
                    outstrm = (OutputStream) msg.obj;
                    System.out.println("SEND_OUTSTREAM=" + outstrm.toString());
                    mState = "UNHINDE_MENU";
                    //supportInvalidateOptionsMenu();//AK: for APK 8 and below

                    invalidateOptionsMenu(); //currently set to 11
                    initialization_paintView();

                    findViewById(R.id.gen_start).setVisibility(View.GONE);
                    findViewById(R.id.start_button).setVisibility(View.VISIBLE);
                    findViewById(R.id.generate_button).setVisibility(View.GONE);
//                    bt_start = (Button) findViewById(R.id.start_button);
//                    bt_start.setOnClickListener(Bluetooth_connect.this);
//                    bt_start_2 = (Button) findViewById(R.id.start_2);
//                    bt_start_2.setOnClickListener(Bluetooth_connect.this);
//
//                    bt_gen = (Button) findViewById(R.id.generate_2);
//                    bt_gen.setOnClickListener(Bluetooth_connect.this);
//                    battery.setTitle("Battery: " + MainActivity.battery_status);

                    //call the method from main activity for creating canvas
                    break;
                case CONNECTION_ERROR_DEVICE:
                    //Hide this Error Message when Device Getting Error in Connection
//                    Toast.makeText(getApplicationContext(), "Bluetooth connection to ECG Recorder failed.", Toast.LENGTH_SHORT).show();
                    break;
                case CONNECTION_ERROR_GENERAL:
                    Toast.makeText(getApplicationContext(), "Selected Device is not a ECG Recorder.", Toast.LENGTH_SHORT).show();
                    break;

                case MESSAGE_COMMUNICATION:
                    AlertDialog.Builder build2 = new AlertDialog.Builder(
                            MainActivity.this);
                    build2.setTitle("Communication Error");
                    build2.setMessage("Communication Error! \n Try Again!");
                    build2.setCancelable(false);
                    build2.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    System.exit(1);
                                }
                            });
                    AlertDialog alert2 = build2.create();
                    alert2.show();
                    //battery.setTitle("Battery: " + MainActivity.battery_status);

                    break;
            }
        }
    };

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
                System.out.println("ConnectedThread");
            } catch (Exception e) {
                System.out.println("Exception in ConnectedThread: + " + e.getMessage());
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            // Keep listening to the InputStream until an exception occurs
            try {
                Message msg1 = Bt_mHandler.obtainMessage(SEND_INSTREAM, mmInStream);
                Bt_mHandler.sendMessage(msg1);
                System.out.println("run ConnectedThread");
                Message msg2 = Bt_mHandler.obtainMessage(SEND_OUTSTREAM, mmOutStream);
                Bt_mHandler.sendMessage(msg2);
                //battery.setTitle("Battery: " + MainActivity.battery_status);

            } catch (Exception e) {
                Message m1 = Bt_mHandler.obtainMessage(MESSAGE_COMMUNICATION);
                Bt_mHandler.sendMessage(m1);
                System.out.println("Error in connected thread=" + e.getMessage());
            }
        }
    }

    //used for scanning the devices on button click(scan button)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {

//        battery.setTitle("Battery: " + MainActivity.battery_status);
//        myVib.vibrate(50);

        if (v.getId() == R.id.generate_2) {
            Create_report_alert();
        } else {
            if (v.getId() == R.id.start_button || v.getId() == R.id.start_2) {
                //myVib.vibrate(50);

                if (started) {
                    stop();
                } else {
                    start();
                }

            } else {
                try {
                    listAdapter.clear();
                    devices.clear();
                    Thread.sleep(2000); // Dhaval1
                    doDiscovery();

                } catch (Exception e) {
                    System.out.println("error in on click" + e.getMessage());
                }
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set device discovery
                    doDiscovery();
                    init();
                    System.out.println("REQUEST_ENABLE_BT");

                } else {
                    // User did not enable Bluetooth or an error occurred
                    System.out.println("bt should be on");
                    Toast.makeText(this, "BLUETOOTH SHOULD BE ENABLED",
                            Toast.LENGTH_LONG).show();
                    System.exit(0);
                }
        }
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket, because mmSocket is final
            BluetoothSocket tmp = null;
            try {
                // MY_UUID is the app's UUID string, also used by the server code
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
                String deviceName = device.getName();
                System.out.println("ConnectThread");
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Socket error: Constructor error", Toast.LENGTH_SHORT).show();
                System.out.println("ERR!=" + e.getMessage());
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it will slow down the connection
            System.out.println("entered run thread, before cancelDiscovery");
            bAdapter.cancelDiscovery();
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    ScanDialog.dismiss();
                    DataDialog.show();
                }
            });

            try {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception

                System.out.println("run ConnectThread");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {

                    e.printStackTrace();
                }
                mmSocket.connect();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        DataDialog.hide();
                    }
                });
            } catch (IOException connectException) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        DataDialog.hide();
                    }
                });
                //Toast.makeText(getApplicationContext(), "Socket error: run() IOException", Toast.LENGTH_SHORT).show();
                System.out.println("IO Exception in run()");
                // Unable to connect; close the socket and get out
                Log.e("Dhaval APP", "I got an error", connectException);

                Log.e("DHAVAL APP", "STACKTRACE");
                Log.e("DHAVAL APP", Log.getStackTraceString(connectException));
                try {
                    mmSocket.close();
                    this.cancel();    //Thread is canceled/closed along with the socket... RKJ
                    System.out.println("Not a ECG Recorder run()");
                    Message msg1 = Bt_mHandler.obtainMessage(CONNECTION_ERROR_DEVICE);
                    Bt_mHandler.sendMessage(msg1);
                } catch (Exception closeException) {
                    Toast.makeText(getApplicationContext(), "Socket error: run() socket close", Toast.LENGTH_SHORT).show();
                    System.out.println("Socket close error in run ERR3=" + closeException.getMessage());
                    Message msg2 = Bt_mHandler.obtainMessage(CONNECTION_ERROR_GENERAL);
                    Bt_mHandler.sendMessage(msg2);
                }

                return;
            }
            // Do work to manage the connection (in a separate thread)
            Message msg = Bt_mHandler.obtainMessage(SUCCESS_CONNECT, mmSocket);
            Bt_mHandler.sendMessage(msg);
        }

        /**
         * Will cancel an in-progress connection, and close the socket
         */

        public void cancel() {
            try {
                mmSocket.close();
                // Stop this Line because it is look like going to infinite loop
//                this.cancel();        //Thread is canceled/closed along with the socket... RKJ
            } catch (IOException e) {
            }
        }
    }

    public void start() {
        //mAnimationSet.pause();
        //findViewById(R.id.effect).setVisibility(View.GONE);
        new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                txt_start_test.setEnabled(false);
                txt_start_test.setText("Stop(" + millisUntilFinished / 1000 + ")");
                txt_start_test.setBackgroundColor(Color.BLACK);
                Log.d("medtel-debug", "timer ticking");
                //startNew();
            }

            public void onFinish() {
                txt_start_test.setEnabled(true);
                txt_start_test.setText("Stop");
                Log.d("medtel-debug", "timer ticking finish stop");
                //txt_start_test.setBackground(getResources().getDrawable(R.drawable.new_main_activity_button_background));
            }
        }.start();
        started = true;
        txt_start_test.setVisibility(View.VISIBLE);
//        Message m7 = mHandler.obtainMessage(START_DATA_ACQ);
//        mHandler.sendMessage(m7);
        //findViewById(R.id.gen_start).setVisibility(View.GONE);

//        if (demo_start == true) {
//            load_existing_file = true;
//            repaint();
//            Demo_read();
//            initialization_paintView();
//            pv.invalidate();
//        } else
//            {
//            pv.first_pass = true;
//            refresh_data = true;
//            load_existing_file = false;
//            stop = false;
//            if (acq_repeat == true)
//                Patient_Info.data_entered = true;
//            else
//
//                Patient_Info.data_entered = false;
//            mode_menu.setVisible(false);
//            gain_menu.setVisible(false);
//            filter_menu.setVisible(false);
//            load_menu.setVisible(false);
//            paintView.batt_count = 0;
//            paintView.Battery_level = 0;
//            //pass message to handler
//            Message m7 = mHandler.obtainMessage(START_DATA_ACQ);
//            mHandler.sendMessage(m7);
//        }

    }

    public void stop() {
        //stopNew();
        Log.d("medtel-debug", "timer stop bitch ");
        started = false;
        txt_start_test.setVisibility(View.GONE);
        //findViewById(R.id.gen_start).setVisibility(View.VISIBLE);
//        //RelativeLayout mylayout = (RelativeLayout) findViewById(R.id.effect2);
//
//        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(mylayout, "alpha", .5f, .1f);
//        fadeOut.setDuration(300);
//        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(mylayout, "alpha", .1f, .5f);
//        fadeIn.setDuration(300);

//
//        mAnimationSet.play(fadeIn).after(fadeOut);
//
//        mAnimationSet.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                super.onAnimationEnd(animation);
//                mAnimationSet.start();
//            }
//        });
//
//        mAnimationSet.start();
//
//        try {
//            outstrm.write('S');    //char 'S' sent to stop acquisition
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        stop = true;
        refresh_data = false;
//        mode_menu.setVisible(true);
//        filter_menu.setVisible(true);
//        gain_menu.setVisible(true);
//        if (mode == 1)
//            filter_menu.setVisible(false);
//        //pass message to handler
//        Message m = mHandler.obtainMessage(STOP_DATA);
//        mHandler.sendMessage(m);
    }

    private void store_bluetooth_address(String bt_address) {
        Log.d("medtel-debug", " store bluetooth address = " + bt_address);
//        File f = new File(settingsFolder, "bluetooth_address.bin");
//        OutputStream osw = null;
//        try {
//            if (!f.exists()) {
//                f.createNewFile();
//            }
//            osw = new FileOutputStream(f);
//            osw.write(bt_address.getBytes());
//            System.out.println("Bytes Written:" + bt_address.getBytes().length);
//            osw.close();
//        } catch (Exception e) {
//            e.printStackTrace(System.err);
//        }
    }

    public void initialization_paintView() {
        try {
            setContentView(R.layout.activity_main_canvas);

            txt_start_test = (Button) findViewById(R.id.start_button);
            txt_start_test.setOnClickListener(this);

            if (load) {
                findViewById(R.id.gen_start).setVisibility(View.GONE);
                findViewById(R.id.start_button).setVisibility(View.GONE);
                findViewById(R.id.generate_button).setVisibility(View.VISIBLE);
                findViewById(R.id.generate_button).setOnClickListener(this);
                findViewById(R.id.effect).setVisibility(View.GONE);
            } else {
                findViewById(R.id.gen_start).setVisibility(View.GONE);
                findViewById(R.id.start_button).setVisibility(View.VISIBLE);
                findViewById(R.id.generate_button).setVisibility(View.GONE);
                RelativeLayout mylayout = (RelativeLayout) findViewById(R.id.effect);
                ObjectAnimator fadeOut = ObjectAnimator.ofFloat(mylayout, "alpha", .5f, .1f);
                fadeOut.setDuration(300);
                ObjectAnimator fadeIn = ObjectAnimator.ofFloat(mylayout, "alpha", .1f, .5f);
                fadeIn.setDuration(300);

                mAnimationSet.play(fadeIn).after(fadeOut);
                mAnimationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mAnimationSet.start();
                    }
                });
                mAnimationSet.start();
            }

            int status_bar_height = getStatusBarHeight();//
            int titlebar_height = (getTitleBarHeight() - status_bar_height);//titlebar_height is the total height of the screen including the titlebar

            //create paintview object......

            pv = new paintView(titlebar_height, status_bar_height, iScreenWidth, instrm, outstrm, MainActivity.this);
            pv.cnvs.drawRGB(0, 0, 0);
            setTitleColor(Color.BLACK);
            set_title(mode, paintView.Auto);
            //total pages to be displayed for different lead selected
            total_pages = paintView.CH_NUM / no_of_lead_to_display;
            pv.drawGridLines(pv.cnvs);
            pv.print_text_on_canvas(paintView.page);

            ActionBar ab = getActionBar();
            //ab.setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bar_background_color));
            ab.setDisplayUseLogoEnabled(false);
            ab.setDisplayShowHomeEnabled(false);
            ab.show();
            LinearLayout ll = (LinearLayout) findViewById(R.id.canvas);
            ll.addView(pv);
            //setContentView(pv);
            pv.requestFocus();
            System.out.println("");
            switch_to_gain();
            if (load_existing_file == false) {

                findViewById(R.id.gen_start).setVisibility(View.VISIBLE);
                findViewById(R.id.generate_button).setVisibility(View.GONE);
                findViewById(R.id.start_button).setVisibility(View.GONE);

                outstrm.write('S');//to stop data ACQ when connection was break
                outstrm.write('R');
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error" + e.getMessage());
        }
    }

    //ask user for type of report to be generated
    public void Create_report_alert() {
        try {
            create_pdf = true;
//            if (Patient_Info.data_entered == false && load_existing_file == false) {
//                onOptionsItemSelected(add_patient_info);
//            }

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Generate Report");
            builder.setMessage("Do you want to generate a pdf report?");
            builder.setCancelable(false);

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    Message report = mHandler.obtainMessage(GENERATE_REPORT);
                    mHandler.sendMessage(report);

                }
            });
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //returns the height of the title bar
    public int getTitleBarHeight() {
        int viewtop = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
        f_size = getResources().getDimensionPixelSize(R.dimen.font_size);
        return (viewtop);
    }

    //this method finds the height of status bar
    public int getStatusBarHeight() {
        Rect r = new Rect();
        Window w = getWindow();
        w.getDecorView().getWindowVisibleDisplayFrame(r);
        return r.top;
    }

    //used for setting the title of the screen
    public void set_title(int mode, boolean auto) {
        String data_str;
        if (mode == 1)
            data_str = "Test";
        else
            data_str = "ECG";

        setTitle(data_str);
    }

    //sends the characters according to the selected gain
    private void switch_to_gain() {
        try {
            switch (iGain) {
                case 1:
                    outstrm.write('B'); //System.out.println("igain method="+iGain);
                    break;
                case 2:
                    outstrm.write('C'); //System.out.println("igain method="+iGain);
                    break;
                case 3:
                    outstrm.write('D'); //System.out.println("igain method="+iGain);
                    break;
                case 4:
                    outstrm.write('E');//System.out.println("igain method="+iGain);
                    break;
                case 6:
                    outstrm.write('F');//System.out.println("igain method="+iGain);
                    break;
                case 8:
                    outstrm.write('G');//System.out.println("igain method="+iGain);
                    break;
                case 12:
                    outstrm.write('H');//System.out.println("igain method="+iGain);
                    break;
            }// switch
        } catch (Exception e) {
            System.out.println("Exception in gain=>" + e.getMessage());
        }
    }

    protected void set_filter(int filter) {
        try {
            switch (filter) {
                case 0:       //System.out.println("0 FIR_B_150 raw");
                    break;    // 0 to 150 Hz (RAW)
                case 1:
                    for (int i = 0; i <= 200; i++) {
                        pv.FIR_B[i] = pv.FIR_B_150[i];
                    }
                    break;    // 0 to 150 Hz (-50Hz)
                case 3:
                    for (int i = 0; i <= 200; i++) {
                        pv.FIR_B[i] = pv.FIR_B_5_40[i];
                    }
                    break;    // 5 to 40 Hz
                case 2:
                    for (int i = 0; i <= 200; i++) {
                        pv.FIR_B[i] = pv.FIR_B_40[i];
                    }
                    break;    // 0 to 40 Hz
                case 4:
                    for (int i = 0; i <= 200; i++) {
                        pv.FIR_B[i] = pv.FIR_B_5_25[i];
                    }
                    break;    // 5 to 25 Hz
                case 5:
                    for (int i = 0; i <= 200; i++) {
                        pv.FIR_B[i] = pv.FIR_B_25[i];
                    }
                    break;    // 0 to 25 Hz
            }// switch
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //handler receives messages and carries out the specified action
    //@SuppressWarnings("deprecation")
    public final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case START_DATA_ACQ:
                    set_empty_arry();
                    if (strcounter == 0) {
                        try {
                            while (instrm.available() > 0) {
                                instrm.read();
                            }
                            switch_to_gain();
                            set_filter(paintView.filter_state);
                            if (pv.debug_mode == true)
                                outstrm.write('U');//for debug mode
                            if (mode == 1)        // Test Channels
                            {
                                outstrm.write('T');
                            } else {
                                outstrm.write('A');
                            }// Take ECG

                            Toast.makeText(getApplicationContext(), "Initialising please wait...", Toast.LENGTH_SHORT).show();
                            pv.init_done = false;
                            pv.array_full = false;
                            pv.report_count = 0;
                            pv.echo.start();
                            strcounter++;
                        } catch (IOException e) {
                            System.out.println("START_DATA error=" + e.getMessage());
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            switch_to_gain();
                            set_filter(paintView.filter_state);
                            if (mode == 1)        // Test Channels
                            {
                                outstrm.write('T');
                                if (paintView.filter_state != 0)
                                    paintView.filter_state = 0;
                            } else               // Take ECG
                                outstrm.write('A');

                            Toast.makeText(getApplicationContext(), "Acquiring data please wait...", Toast.LENGTH_SHORT).show();
                            pv.init_done = false;
                            pv.plot_done = false;
                            paintView.roll_over = false;
                            paintView.disp_count = 0;
                            paintView.chk_disp_count = 0;
                            paintView.ADS_samples_count = 0;
                            paintView.check_data_count = 0;
                            paintView.fill_count = 0;
                            paintView.Max_Scaling_factor = 0;
                            paintView.Min_Scaling_factor = 0;
                            pv.report_count = 0;
                            pv.onResume();
                        } catch (Exception e) {
                            System.out.println("START_DATA error=" + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                    break;
                case STOP_DATA:
                    try {
                        pv.onPause();
                        if (pv.report_count > 0) {
                            paintView.generate_report_opt = true;
                            pv.store_Array(paintView.ADS_samples_count);
                        } else {

                            findViewById(R.id.generate_2).setVisibility(View.GONE);
                            findViewById(R.id.effect2).setVisibility(View.GONE);

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Insufficient data and report cannot be generated", Toast.LENGTH_SHORT).show();
                                    handler.postDelayed(this, 1000);
                                }
                            }, 1000);
                        }

                        if (paintView.generate_report_opt == true)

                            pv.last_position(paintView.disp_count);
                    } catch (Exception e) {
                        System.out.println("STOP_DATA error=" + e.getMessage());
                        e.printStackTrace();
                    }
                    break;
                case NEW_PATIENT:
                    try {

                        //mode_menu.setVisible(true);
                        //filter_menu.setVisible(true);
                        //gain_menu.setVisible(true);
                        //new_patient.setVisible(false);
                        pv.invalidate();
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("error in NEW_PATIENT" + e.getMessage());
                    }
                    break;
                case GENERATE_REPORT:
                    try {
//                        mode_menu.setVisible(false);
//                        filter_menu.setVisible(false);
//                        gain_menu.setVisible(false);
//                        new_patient.setVisible(true);
//                        //repeat_acq.setVisible(true);
                        report_gen = true;
//                        comment = "";
//
//                        cr = new Create_report(MainActivity.this);
//                        Open_view_report();
                    } catch (Exception e) {
                        System.out.println("error in GENERATE_REPORT" + e.getMessage());
                    }
                    break;
                case LOAD_DATA:
                    onOptionsItemSelected(load_menu);
                    break;
                case MESSAGE_COMMUNICATION:
                    AlertDialog.Builder build2 = new AlertDialog.Builder(MainActivity.this);
                    build2.setTitle("Communication Error");
                    build2.setMessage("Communication Error! \n Try Again!");
                    build2.setCancelable(false);
                    build2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int arg1) {
                            System.exit(3);
                        }
                    });
                    AlertDialog alert2 = build2.create();
                    alert2.show();
                    break;
            }
        }
    };

    @SuppressWarnings("static-access")
    protected void set_empty_arry() {
        pv.initialise_array();
        paintView.disp_count = 0;
        paintView.chk_disp_count = 0;
        paintView.ADS_samples_count = 0;
        paintView.check_data_count = 0;
        paintView.fill_count = 0;
        pv.batt_count = 0;
        paintView.generate_report_opt = false;
    }

    //displays the pairing window for connectivity
    public void pairDevice(final BluetoothDevice device) {
        try {
            bAdapter.cancelDiscovery();
            Method m = device.getClass().getMethod("createBond", (Class[]) null);
            m.invoke(device, (Object[]) null);
            device_address = device.getAddress().toString();
            System.out.println("pairing device");


            String demo = device.getName();
            Log.e("===>>DEMO", "pairDevice: " + demo);

            if (demo.startsWith("ECGREC-DEMO")) {

                String inputDate = demo.replaceAll("[^0-9]", "");

                SimpleDateFormat inputdate = new SimpleDateFormat("yyMMdd");
                SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date outDate = null;
                try {
                    outDate = inputdate.parse(inputDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String outputText = outputFormat.format(outDate);
                String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());

                Date current = null;
                Date outPut = null;
                try {
                    current = outputFormat.parse(currentDate);
                    outPut = outputFormat.parse(outputText);
                } catch (ParseException e) {
                    e.printStackTrace();

                }

                if (outPut.equals(current) || outPut.compareTo(current) < 0) {

                    builder.setMessage("Please return the Demo piece to the company").setCancelable(false);
                    builder.setTitle("Alert!");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();

                } else if (current.compareTo(outDate) < 0) {

                    long remainingTime = outDate.getTime() - current.getTime();
                    long days = remainingTime / (24 * 60 * 60 * 1000);

                    builder.setMessage("You have " + days + " days left for demo ").setCancelable(false);
                    builder.setTitle("Alert!");

                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ConnectThread connect = new ConnectThread(device);
                            connect.start();
                        }
                    });
                    AlertDialog alert = builder.create();

                    alert.show();
                }
            } else {
                ConnectThread connect = new ConnectThread(device);
                connect.start();
            }

        } catch (Exception e) {
            System.out.println("ERROR PAIR DEVICE " + e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//		MenuInflater inf=getMenuInflater();
//		inf.inflate(R.menu.main, menu);
        new MenuInflater(getApplication()).inflate(R.menu.main, menu);


        //get the position of menu options
        battery = menu.getItem(0);
        prev_menu = menu.getItem(1);
        device_detail_menu = menu.getItem(3);
        next_menu = menu.getItem(2);
        mode_menu = menu.getItem(4);
        filter_menu = menu.getItem(5);
        gain_menu = menu.getItem(6);
        acq_mode = menu.getItem(7);
        add_patient_info = menu.getItem(8);
        load_menu = menu.getItem(9);
        new_patient = menu.getItem(10);
        prev_lead = menu.getItem(11);
        next_lead = menu.getItem(12);
        //repeat_acq = menu.getItem(13);

        battery.setVisible(false);
        filter_menu.setVisible(false);
        mode_menu.setVisible(false);
        device_detail_menu.setVisible(true);
        gain_menu.setVisible(false);
        load_menu.setVisible(false);
        prev_menu.setVisible(false);
        next_menu.setVisible(false);
        new_patient.setVisible(false);
        next_lead.setVisible(false);
        prev_lead.setVisible(false);
        //repeat_acq.setVisible(false);

        //hides the menu during connectivity
        if (mState == "HIDE_MENU") {
            for (int j = 0; j < menu.size(); j++) {
                menu.getItem(j).setVisible(false);
            }
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (REGISTER) {
            unregisterReceiver(receiver);
            REGISTER = false;
        }
    }

    protected void onDestroy() {
        // Make sure we're not doing discovery anymore
        if (bAdapter != null) {
            bAdapter.cancelDiscovery();
            // Unregister broadcast listeners
            if (REGISTER)
                this.unregisterReceiver(receiver);
        }
        Toast.makeText(this, "Quitting App", Toast.LENGTH_LONG).show();
        super.onDestroy();
        System.exit(2);
    }

    public void startNew() {
        mAnimationSet.pause();
        findViewById(R.id.effect).setVisibility(View.GONE);
        new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                txt_start_test.setEnabled(false);
                txt_start_test.setText("Stop(" + millisUntilFinished / 1000 + ")");
                txt_start_test.setBackgroundColor(Color.BLACK);
            }

            public void onFinish() {
                txt_start_test.setEnabled(true);
                txt_start_test.setText("Stop");
                //bt_start.setBackground(getResources().getDrawable(R.drawable.new_main_activity_button_background));
            }
        }.start();
        started = true;
        txt_start_test.setVisibility(View.VISIBLE);
        findViewById(R.id.gen_start).setVisibility(View.GONE);

        if (demo_start == true) {
            load_existing_file = true;
            repaint();
            Demo_read();
            initialization_paintView();
            pv.invalidate();
        } else {
            pv.first_pass = true; //visible first
            refresh_data = true;
            load_existing_file = false;
            stop = false;
//            if (acq_repeat == true)
//                Patient_Info.data_entered = true;
//            else
//
//                Patient_Info.data_entered = false;
            mode_menu.setVisible(false);
            gain_menu.setVisible(false);
            filter_menu.setVisible(false);
            load_menu.setVisible(false);
            paintView.batt_count = 0;
            paintView.Battery_level = 0;
            //pass message to handler
            Message m7 = mHandler.obtainMessage(START_DATA_ACQ);
            mHandler.sendMessage(m7);
        }

    }

    public void stopNew() {
        started = false;
        txt_start_test.setVisibility(View.GONE);
        findViewById(R.id.gen_start).setVisibility(View.VISIBLE);
        RelativeLayout mylayout = (RelativeLayout) findViewById(R.id.effect2);

        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(mylayout, "alpha", .5f, .1f);
        fadeOut.setDuration(300);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(mylayout, "alpha", .1f, .5f);
        fadeIn.setDuration(300);


        mAnimationSet.play(fadeIn).after(fadeOut);

        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimationSet.start();
            }
        });

        mAnimationSet.start();

        try {
            outstrm.write('S');    //char 'S' sent to stop acquisition
        } catch (IOException e) {
            e.printStackTrace();
        }
        stop = true;
        refresh_data = false;
        mode_menu.setVisible(true);
        filter_menu.setVisible(true);
        gain_menu.setVisible(true);
        if (mode == 1)
            filter_menu.setVisible(false);
        //pass message to handler
        Message m = mHandler.obtainMessage(STOP_DATA);
        mHandler.sendMessage(m);
    }

    //redraw the screen view
    public void repaint() {
        if (menu_clicked == true) {
            if (pv.bitmap != null)
                pv.cnvs.drawBitmap(pv.bitmap, 0, 0, pv.paint);

            pv.bitmap.eraseColor(Color.TRANSPARENT);
            pv.cnvs.drawRGB(0, 0, 0);
            pv.drawGridLines(pv.cnvs);
            pv.print_text_on_canvas(paintView.page);
            menu_clicked = false;
        }

        if (next_and_prev_clicked != true) {
            paintView.plot_on = false;
            pv.initialise_array();
        } else
            paintView.plot_on = true;


        //set_title(mode,false);
        set_title(mode, paintView.Auto);
        //pv.invalidate();
        paintView.disp_count = 0;
        paintView.chk_disp_count = 0;
        paintView.ADS_samples_count = 0;
        paintView.check_data_count = 0;
        paintView.batt_count = 0;
        next_and_prev_clicked = false;
    }

    private void Demo_read() {
        try {
            //opens the demo file stored in the asset folder of application...file included in the application apk file
            AssetManager manager = getAssets();
            InputStream mInput = manager.open("Demo.dat");
            DataInputStream dinstream = new DataInputStream(mInput);

            //reads the other details
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            dinstream.readUTF();
            for (int j = 0; j < 12; j++)//reads the ecg data and saves in the raw_data array
            {
                for (int k = 0; k < paintView.total_samples; k++) {
                    paintView.raw_data[j][k] = (short) dinstream.readInt();
                }
            }
            dinstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}